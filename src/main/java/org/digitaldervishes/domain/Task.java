package org.digitaldervishes.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Task {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	@XmlTransient
	private int id;
	private String description;
	
	@OneToOne
	 @JoinTable(name = "task_skill", 
	   joinColumns = @JoinColumn(name = "taskid"), 
	   inverseJoinColumns = @JoinColumn(name = "sessionid")
	 )
	private Skill skill;
	
	public Task() {
		
	
	}
	public Task(String description, Skill skill){
		
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Skill getSkill() {
		return skill;
	}
	public void setSkill(Skill skill) {
		this.skill = skill;
	}
	
	
	
	
	

}
