package org.digitaldervishes.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Project {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	@XmlTransient
	private int id;
	
	
	private String description;
	
	@OneToMany(fetch = FetchType.EAGER ,cascade = CascadeType.ALL)
	private List<Task> tasks;
	
	public void addTask()
	{
		
	}
	
	public Project ()
	{
		
	}
	
	public Project(String description){
		this.description = description;
	}
	
	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void addTask(Task task) {
		tasks.add(task);
		
		
	}
	

}
