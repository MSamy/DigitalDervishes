package org.digitaldervishes.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import org.digitaldervishes.dao.BaseDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseDaoImpl<T> implements BaseDao<T> {

	@Autowired
	protected SessionFactory sessionFactory;

	private Class<T> type;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public BaseDaoImpl() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		type = (Class) pt.getActualTypeArguments()[0];
	}

	@Override
	public T add(T t) {
		sessionFactory.getCurrentSession().save(t);
		return t;
	}

	@Override
	public T update(T t) {
		sessionFactory.getCurrentSession().saveOrUpdate(t);
		return t;
	}

	@Override
	public boolean delete(long id) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(session.load(type, id));
		return true;
	}

	@Override
	public T get(long id) {
		return sessionFactory.getCurrentSession().get(type, id);
	}
}
