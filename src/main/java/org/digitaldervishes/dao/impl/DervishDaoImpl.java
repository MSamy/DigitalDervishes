package org.digitaldervishes.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.digitaldervishes.dao.DervishDao;
import org.digitaldervishes.domain.Dervish;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("dervishDao")
public class DervishDaoImpl extends BaseDaoImpl<Dervish> implements DervishDao{
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Dervish> getAll() {
		Query query = sessionFactory.getCurrentSession().createQuery("SELECT d from Dervish d");
		return new ArrayList<Dervish>(query.list());
	}

}
