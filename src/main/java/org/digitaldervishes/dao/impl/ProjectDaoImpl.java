package org.digitaldervishes.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.digitaldervishes.dao.ProjectDao;
import org.digitaldervishes.domain.Dervish;
import org.digitaldervishes.domain.Project;
import org.hibernate.Query;

public class ProjectDaoImpl extends BaseDaoImpl<Project>  implements ProjectDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<Project> getAll() {
		Query query = sessionFactory.getCurrentSession().createQuery("SELECT p from Project p");
		return new ArrayList<Project>(query.list());
	}

}
