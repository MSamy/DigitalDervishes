package org.digitaldervishes.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.digitaldervishes.dao.CustomerDao;
import org.digitaldervishes.domain.Customer;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("customerDao")
public class CustomerDaoImpl extends BaseDaoImpl<Customer> implements CustomerDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<Customer> getAll() {
		Query query = sessionFactory.getCurrentSession().createQuery("SELECT c from Customer c");
		return new ArrayList<Customer>(query.list());
	}

}
