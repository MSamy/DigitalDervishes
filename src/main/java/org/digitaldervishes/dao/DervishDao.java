package org.digitaldervishes.dao;

import java.util.List;

import org.digitaldervishes.domain.Dervish;

public interface DervishDao extends  BaseDao<Dervish>{

	List<Dervish> getAll();

}
