package org.digitaldervishes.dao;

import java.util.List;

import org.digitaldervishes.domain.Dervish;
import org.digitaldervishes.domain.Project;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectDao {

	List<Project> getAll();
}
