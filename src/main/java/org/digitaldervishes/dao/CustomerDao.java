package org.digitaldervishes.dao;

import java.util.List;

import org.digitaldervishes.domain.Customer;

public interface CustomerDao extends BaseDao<Customer>{

	List<Customer> getAll();

}
