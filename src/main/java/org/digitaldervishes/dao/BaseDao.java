package org.digitaldervishes.dao;

public interface BaseDao<T> {

	T add(T t);

	T update(T t);

	boolean delete(long id);

	T get(long id);

}
