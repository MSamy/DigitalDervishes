package org.digitaldervishes.config;


import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;
import org.digitaldervishes.domain.Customer;
import org.digitaldervishes.service.CustomerService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * Servlet Context configuration
 * 
 * @author MSamy
 * 
 * 
 *
 */
@Configuration
@EnableWebMvc
@EnableTransactionManagement
//@ComponentScan("org.digitaldervishes.*")
@ComponentScan({"org.digitaldervishes.config","org.digitaldervishes.service", "org.digitaldervishes.dao", "org.digitaldervishes.dao.impl", "org.digitaldervishes.controller"})
public class ServletConfig extends WebMvcConfigurerAdapter {

	@Value("${db.driver.class.name}")
	private String driverClassName;

	@Value("${db.url}")
	private String url;

	@Value("${db.username}")
	private String dbUsername;

	@Value("${db.password}")
	private String dbPassword;

	@Bean
	public static PropertyPlaceholderConfigurer properties() {
		PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
		Resource[] resources = new ClassPathResource[] { new ClassPathResource("digitaldervishes.properties") };
		ppc.setLocations(resources);
		ppc.setIgnoreResourceNotFound(false);
		ppc.setIgnoreUnresolvablePlaceholders(false);
		return ppc;
	}

	@Bean(name = "dataSource")
	public BasicDataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(driverClassName);
		dataSource.setUrl(url);
		dataSource.setUsername(dbUsername);
		dataSource.setPassword(dbPassword);
		return dataSource;
	}

	@Bean
	public SessionFactory sessionFactory() {
		LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(dataSource());
		builder.scanPackages("org.digitaldervishes.domain").addProperties(getHibernateProperties());

		return builder.buildSessionFactory();
	}
	
	

	private Properties getHibernateProperties() {
		Properties prop = new Properties();
		prop.put("hibernate.format_sql", "true");
		prop.put("hibernate.show_sql", "true");
		prop.put("hibernate.hbm2ddl.auto", "create");
		prop.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		return prop;
	}

	/**
	 * For the messages.
	 * @return
	 */
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource bundle = new ReloadableResourceBundleMessageSource();
	
		bundle.setBasename("classpath:messages");
		//bundle.setBasenames(new String[] {"WEB-INF/i18n/messages"});
		bundle.setCacheSeconds(1);
		bundle.setFallbackToSystemLocale(true);
		return bundle;
	}
	
	/**
	 * To save the locale in a cookie on the browser, and not in the session.
	 * @return
	 */
	@Bean
	public CookieLocaleResolver localeResolver() {
		CookieLocaleResolver localeResolver = new CookieLocaleResolver();
		
		localeResolver.setDefaultLocale(Locale.ENGLISH);
		localeResolver.setCookieName("locale");
		return localeResolver;
	}
	
	@Bean
	@Autowired
	public HibernateTransactionManager txManager(SessionFactory sf) {
		return new HibernateTransactionManager(sf);
	}
	
	  	@Bean
	    public InternalResourceViewResolver jspViewResolver() {
	        InternalResourceViewResolver bean = new InternalResourceViewResolver();
	        bean.setPrefix("/WEB-INF/view/");
	        bean.setSuffix(".jsp");
	        return bean;
	    }
	  	
	  	
}
