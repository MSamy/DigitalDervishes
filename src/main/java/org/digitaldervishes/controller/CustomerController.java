/*package org.digitaldervishes.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.digitaldervishes.domain.Address;
import org.digitaldervishes.domain.Customer;
import org.digitaldervishes.domain.Project;
import org.digitaldervishes.domain.Skill;
import org.digitaldervishes.domain.Task;
import org.digitaldervishes.service.CustomerService;
import org.digitaldervishes.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public class CustomerController {
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	ProjectService projectService;
	
	@PostConstruct
	public void populatecustomers() {
		List<Customer> customers = new ArrayList<>();
		customers.add(new Customer("Genkis", "Khan", "1234567890", new Address("Fairfield", "IA", "1000 N St", 52557, "USA")));
		customers.add(new Customer("Vladimir", "Putin", "12312414232", new Address("Thames", "BG", "1000 N St", 23462, "UK")));
		customers.add(new Customer("Micheal", "Jackson", "1325112145",	new Address("MorganTown", "WV", "2605 Chestnut Hls", 26505, "USA")));
		customerService.saveAll(customers);
		Customer c1 =customers.get(0);
		
		c1.addProject(new Project("Website"));
		c1.getProjects().get(0).addTask(new Task("Web Design", new Skill("HTML")));
		c1.getProjects().get(0).addTask(new Task("Web Development", new Skill("Python")));
		c1.getProjects().get(0).addTask(new Task("Database Development", new Skill("SQL")));
		
		
		customerService.saveAll(customers);
	}

	*//**
	 * @param model
	 * list all parameters
	 * @return
	 *//*
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String listcustomeres(Model model) {
		System.out.println("List customeres controller");
		model.addAttribute("customer", customerService.getAllCustomers());

		return "customers";

	}
	

}
*/