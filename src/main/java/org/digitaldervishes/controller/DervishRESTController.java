/*package org.digitaldervishes.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.digitaldervishes.domain.Address;
import org.digitaldervishes.domain.Dervish;

import org.digitaldervishes.service.DervishService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
// @RestController This also can be used instead of @Controller so that we don't
// have to user @ResponseBody annotation.
@RequestMapping("rest/dervishes")
public class DervishRESTController {

	@Autowired
	private DervishService dervishService;

	@PostConstruct
	public void populatedervishs() {
		List<Dervish> dervishes = new ArrayList<>();
		dervishes.add(new Dervish("Anil", "K C", "1234567890", new Address("Fairfield", "IA", "1000 N St", 52557, "USA")));
		dervishes.add(new Dervish("Kamal", "Nepal", "12312414232", new Address("Thames", "BG", "1000 N St", 23462, "UK")));
		dervishes.add(new Dervish("Amit", "K C", "1325112145",	new Address("MorganTown", "WV", "2605 Chestnut Hls", 26505, "USA")));
		dervishService.saveAll(dervishes);
	}

	@RequestMapping(value = { "/list" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	//@ResponseBody
	public List<Dervish> getAlldervishs() {
		return dervishService.getAllDervishes();
	}
	
	@RequestMapping(value = { "/REST", "" }, method = RequestMethod.GET)
	public String getAlldervishs(Model model) {
		model.addAttribute("dervishes", dervishService.getAllDervishes()) ;
		return "dervishs";
	}

	@RequestMapping(value = "/{dervishId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	//@ResponseBody
	public Dervish getdervish(@PathVariable long dervishId) {
		return dervishService.getdervishById(dervishId);
	}

	@RequestMapping(value = "/{dervishId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	//@ResponseBody
	public ResponseEntity<String> deletedervish(@PathVariable long dervishId) {

		JSONObject responseBody = new JSONObject();
		HttpStatus httpStatus = HttpStatus.OK;

		if (dervishService.deletedervishById(dervishId)) {

			responseBody.put("message", "Successfully deleted");
			responseBody.put("dervishId", dervishId);

		} else {

			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			responseBody.put("message", "Unable to delete due internal server error");
			responseBody.put("dervishId", dervishId);

		}
		return new ResponseEntity<String>(responseBody.toString(), httpStatus);
	}

	@RequestMapping(value = "/{dervishId}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	//@ResponseBody
	public ResponseEntity<String> updatedervish(@PathVariable long dervishId, @RequestBody Dervish dervish) {

		Dervish saveddervish = dervishService.getdervishById(dervishId);

		JSONObject responseBody = new JSONObject();
		HttpStatus httpStatus = HttpStatus.OK;

		if (saveddervish == null) {

			responseBody.put("message", "dervish not found");
			responseBody.put("dervishId", dervishId);
			httpStatus = HttpStatus.BAD_REQUEST;

		} else {

			saveddervish.setFirstName(dervish.getFirstName());
			saveddervish.setLastName(dervish.getLastName());
			saveddervish.setPhoneNumber(dervish.getPhoneNumber());
			saveddervish = dervishService.updateDervish(saveddervish);

			responseBody.put("message", "dervish successfully updated");
			responseBody.put("dervish", new JSONObject(saveddervish));
		}
		return new ResponseEntity<String>(responseBody.toString(), httpStatus);
	}

	@RequestMapping(value = { "/", "" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	//@ResponseBody
	public ResponseEntity<String> adddervish(@RequestBody Dervish dervish) {

		JSONObject responseBody = new JSONObject();
		HttpStatus httpStatus = HttpStatus.OK;

		dervish = dervishService.addDervish(dervish);
		responseBody.put("message", "dervish successfully added");
		responseBody.put("dervish", new JSONObject(dervish));

		return new ResponseEntity<String>(responseBody.toString(), httpStatus);
	}

	@RequestMapping(value = "/{dervishId}/addresses", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	//@ResponseBody
	public ResponseEntity<String> adddervishAddress(@PathVariable long dervishId, @RequestBody Address address) {

		Dervish saveddervish = dervishService.getdervishById(dervishId);

		JSONObject responseBody = new JSONObject();
		HttpStatus httpStatus = HttpStatus.OK;

		if (saveddervish == null) {

			responseBody.put("message", "dervish not found");
			responseBody.put("dervishId", dervishId);
			httpStatus = HttpStatus.BAD_REQUEST;

		} else {
			saveddervish.setAddress(address);
			saveddervish = dervishService.updateDervish(saveddervish);

			responseBody.put("message", "dervish address successfully added");
			responseBody.put("dervishId", dervishId);
			responseBody.put("address", new JSONObject(saveddervish.getAddress()));
		}

		return new ResponseEntity<String>(responseBody.toString(), httpStatus);
	}

	@RequestMapping(value = "/{dervishId}/addresses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	//@ResponseBody
	public ResponseEntity<String> getdervishAddress(@PathVariable long dervishId) {

		Dervish saveddervish = dervishService.getdervishById(dervishId);

		JSONObject responseBody = new JSONObject();
		HttpStatus httpStatus = HttpStatus.OK;

		if (saveddervish == null) {

			responseBody.put("message", "dervish not found");
			responseBody.put("dervishId", dervishId);
			httpStatus = HttpStatus.BAD_REQUEST;

		} else {
			responseBody.put("dervishId", dervishId);
			responseBody.put("address", new JSONObject(saveddervish.getAddress()));
		}

		return new ResponseEntity<String>(responseBody.toString(), httpStatus);
	}

	@RequestMapping(value = "/{dervishId}/xml", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	//@ResponseBody
	public Dervish getdervishInXML(@PathVariable long dervishId) {
		return dervishService.getdervishById(dervishId);
	}

}
*/