/*package org.digitaldervishes.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.digitaldervishes.domain.Address;
import org.digitaldervishes.domain.Dervish;
import org.digitaldervishes.domain.Skill;
import org.digitaldervishes.service.DervishService;
import org.digitaldervishes.service.impl.DervishServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/dervishes")
public class DervishController {
	
	@Autowired
	DervishService dervishService;
	
	@PostConstruct
	public void populateDervishes() {
		List<Dervish> dervishes = new ArrayList<>();
		dervishes.add(new Dervish("Rumi", "Jalaluddin","Rumi", new Address("Fairfield", "IA", "1000 N St", 52557, "USA")));
		dervishes.add(new Dervish("Attar", "Fariduddin","Attar", new Address("Thames", "BG", "1000 N St", 23462, "UK")));
		dervishes.add(new Dervish("Shems", "AlTabrizi","Shems",	new Address("MorganTown", "WV", "2605 Chestnut Hls", 26505, "USA")));
		Dervish d1 =dervishes.get(0);
		d1.addSkill(new Skill("Python"));
		Dervish d2 =dervishes.get(1);
		d2.addSkill(new Skill("Java"));
		Dervish d3 =dervishes.get(2);
		d3.addSkill(new Skill("Haskell"));
		
		dervishService.saveAll(dervishes);
	}

	*//**
	 * @param model
	 * list all parameters
	 * @return
	 *//*
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String listDervishes(Model model) {
		System.out.println("List Dervishes controller");
		model.addAttribute("dervishes", dervishService.getAll());

		return "dervishes";

	}
	

}
*/