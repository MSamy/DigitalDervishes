package org.digitaldervishes.service;

import java.util.List;

import org.digitaldervishes.domain.Customer;

public interface CustomerService {

	Customer addCustomer(Customer customer);

	Customer updateCustomer(Customer customer);

	boolean deleteCustomerById(long id);

	Customer getCustomerById(long id);

	List<Customer> getAllCustomers();

	boolean saveAll(List<Customer> customers);

	
	
}
