package org.digitaldervishes.service;

import java.util.List;

import org.digitaldervishes.domain.Project;

public interface ProjectService {
	
	List<Project> getAll();

	void saveAll(List<Project> projects);

}
