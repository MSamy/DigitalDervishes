
package org.digitaldervishes.service;
import java.util.List;

import org.digitaldervishes.domain.Dervish;

public interface DervishService {

	
	List<Dervish> getAll();

	void saveAll(List<Dervish> dervishes);

	List<Dervish> getAllDervishes();

	Dervish getdervishById(long dervishId);

	boolean deletedervishById(long dervishId);

	Dervish updateDervish(Dervish saveddervish);

	Dervish addDervish(Dervish dervish);

}
