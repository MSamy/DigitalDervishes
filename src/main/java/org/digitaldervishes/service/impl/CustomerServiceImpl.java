package org.digitaldervishes.service.impl;

import java.util.List;

import org.digitaldervishes.dao.CustomerDao;
import org.digitaldervishes.domain.Customer;
import org.digitaldervishes.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("customerService")
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDao customerDao;

	@Override
	public Customer addCustomer(Customer customer) {
		return customerDao.add(customer);
	}

	@Override
	public Customer updateCustomer(Customer customer) {
		return customerDao.update(customer);
	}

	@Override
	public boolean deleteCustomerById(long id) {
		return customerDao.delete(id);
	}

	@Override
	public Customer getCustomerById(long id) {
		return customerDao.get(id);
	}

	@Override
	public List<Customer> getAllCustomers() {
		return customerDao.getAll();
	}

	@Override
	public boolean saveAll(List<Customer> customers) {
		for (Customer customer : customers) {
			addCustomer(customer);
		}
		return false;
	}

}
