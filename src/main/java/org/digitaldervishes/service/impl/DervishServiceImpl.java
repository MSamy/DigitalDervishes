package org.digitaldervishes.service.impl;

import java.util.List;

import org.digitaldervishes.dao.DervishDao;
import org.digitaldervishes.domain.Customer;
import org.digitaldervishes.domain.Dervish;
import org.digitaldervishes.service.DervishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("dervishService")
public class DervishServiceImpl implements DervishService {

	@Autowired
	DervishDao dervishdao;
	@Override
	public List<Dervish> getAll() {
		
		return dervishdao.getAll();
	}

	@Override
	public void saveAll(List<Dervish> dervishes) {
		for (Dervish dervish : dervishes) {
			addDervish(dervish);
		}
		
		
	}

	

	@Override
	public List<Dervish> getAllDervishes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Dervish getdervishById(long dervishId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deletedervishById(long dervishId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Dervish updateDervish(Dervish saveddervish) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Dervish addDervish(Dervish dervish) {
		// TODO Auto-generated method stub
		return null;
	}

}
