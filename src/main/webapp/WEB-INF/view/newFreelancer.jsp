<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/ajax.js"/>"></script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tell us about yourself</title>

</head>
<body>

<section>
		<div>
			<div>
				<h1><spring:message code="newFreelancer.h1" /> </h1>
				<p><p>
			</div>
		</div>
</section>

<section class = "container">
	
  	<form:form  id = "dervishform" modelAttribute="newDervish" method = "post" enctype="multipart/form-data">
	
		<fieldset>
				<p><form:errors path="*" cssStyle="color : red;" /></p>
				<legend>Add new Dervish</legend>
				<div>
					<p>
 					<form:input path="photo" type="file"  />
<!--  <input type="file" name="photo" size="50" />
 -->						
					</p>
				</div>
				<div>
					<label for="title"> <spring:message code = "addFreelancer.form.JobTitle.label" /> </label>
					<p>
						<form:input id="title" path="title" type="text" />
						<form:errors path="title" cssStyle="color : red;"/>
					</p>
				</div>
				<div>
					<label class="control-label col-lg-2 col-lg-2" for="skills"> <spring:message code = "addFreelancer.form.Skills.label" /></label>
					<p>
						<form:input id="skills" path="skills" type="text"/>
						<form:errors path="skills" cssStyle="color : red;"/>
					</p>
				</div>
				
				<div>
					<label for="skills"> <spring:message code = "addFreelancer.form.AboutMe.label" /></label>
					<div>
						<form:input id="description" path="description" type="text"/>
						<form:errors path="description" cssStyle="color : red;"/>
					</div>
				</div>
				
				<div>
					<p>
						<input type="submit" id="btnAdd" value =<spring:message code = "addFreelancer.form.Add.button"/>/>
						<input type="button" value="Ajax Submit" onclick="makeAjaxCall();">
					</p>
				</div>
				
		</fieldset>
	</form:form>  


</section>
</body>
</html>