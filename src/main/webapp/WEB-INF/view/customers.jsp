<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customers</title>
</head>
<body>
	<section>
		<div class="container">
			<h1>Our Customers</h1>
			<p>All of the contributors to our Community!!!</p>
			
				<div> <h3>${SpecialBlurb}</h3> </div>
		</div>
	</section>

	<section class="container">
		<div>
			<c:forEach items="${customers}" var="customer">
				<div style="padding-bottom: 15px">
					<div>
 						<div class="caption">
 						<!--  <img src="<c:url value="/resources/images/${dervish.id}.png"></c:url>" alt="image"  style = "width:100px"/>-->
 						<h3>Customer info</h3>
 							<h4>First Name-  ${customer.firstName}</h4>
							<h4>Last Name - ${customer.lastName}</h4>
							
						<h3>Customer Projects</h3>
						<c:forEach items="${customer.projects}" var="skill">
							<h4>Skill:  $project.description}</h4>
							
						</c:forEach>				
 			<!--  	<a href="<spring:url value="/freelancers/${freelancer.id}" />"> 
							<input type="button" id="Cancel" value="View Detail"/>
						</a>-->
 					</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</section>
</body>
</html>