<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Members</title>
</head>
<body>
	<section>
		<div class="container">
			<h1>Our Dervishes</h1>
			<p>All of the contributors to our Community!!!</p>
			
				<div> <h3>${SpecialBlurb}</h3> </div>
		</div>
	</section>

	<section class="container">
		<div>
			<c:forEach items="${dervishes}" var="dervish">
				<div style="padding-bottom: 15px">
					<div>
 						<div class="caption">
 						<!--  <img src="<c:url value="/resources/images/${dervish.id}.png"></c:url>" alt="image"  style = "width:100px"/>-->
 						<h3>Dervish info</h3>
 							<h4>First Name-  ${dervish.firstName}</h4>
							<h4>Last Name - ${dervish.lastName}</h4>
							<h4>Last Name - ${dervish.nickName}</h4>
						<h3>Dervish Skills</h3>
						<c:forEach items="${dervish.skills}" var="skill">
							<h4>Skill:  ${skill.description}</h4>
							
						</c:forEach>				
 			<!--  	<a href="<spring:url value="/freelancers/${freelancer.id}" />"> 
							<input type="button" id="Cancel" value="View Detail"/>
						</a>-->
 					</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</section>
</body>
</html>