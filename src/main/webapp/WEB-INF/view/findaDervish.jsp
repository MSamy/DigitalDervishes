<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Find a skilled Dervish</title>
</head>
<body>
	<div class="search">
<%-- 		<form:form action="search_dervish" method="POST">
			<form:input path="search" />
			<input type="submit" value="Search" />
		</form:form>  --%>
	</div>
	<center>
		<h1>Browse our Dervishes</h1>
	</center>


	<div id="body_job">
		<c:forEach items="${dervishes }" var="dervish">
			<div class="dervish_output">
				<center>
 						<img src="<c:url value="/resources/images/${dervish.id}.png"></c:url>" alt="image"  style = "width:100px"/><br />
					<label for="name" class="dervish_name">${dervish.member.firstName }
						${dervish.member.lastName} - $${dervish.rate }/hour</label><br /> <label
						for="">${dervish.title }</label><br /> <label for="skills"
						class="dervish_skills">${dervish.skills}</label><br /> <label
						for="">${dervish.description }</label><br />
						
						<a href="<spring:url value="/dervishs/${dervish.id}" />"> 
							<input type="button" id="Cancel" value="View Detail"/>
						</a>
						
				</center>
			</div>
		</c:forEach>
	</div>

</body>
</html>