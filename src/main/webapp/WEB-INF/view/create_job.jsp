<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<spring:url value="/resources/css/style.css" var="mainCss" />
<link href="${mainCss}" rel="stylesheet" />
<title><spring:message code="view.create_job.title" /></title>
</head>
<body>

	<div class="center">
		<spring:message code="view.create_job.header"></spring:message>
	</div>

	<form:form modelAttribute="newJob">
		<div class="form-group">
			<label for="name"> <spring:message
					code="view.create_job.name" /></label>
			<div >
				<form:input id="name" path="name" type="text"
					class="form:input-large" />
				<form:errors path="name" cssClass="text-danger" />
			</div>
		</div>
		<div class="form-group">
			<label for=category>
				<spring:message code="view.create_job.category" />
			</label>
			<div class="col-lg-10">
				<form:input id="category" path="category" type="text"
					/>
				<form:errors path="category" />
			</div>
		</div>
		<div class="form-group">
			<label for="description">
				<spring:message code="view.create_job.description" />
			</label>
			<div>
				<form:input id="description" path="description" type="text"
					size="20"  />
				<form:errors path="description" />
			</div>
		</div>
		<div class="form-group">
			<label for="skills">
				<spring:message code="view.create_job.skills" />
			</label>
			<div>
				<form:input id="skills" path="skills" type="text"
					/>
				<form:errors path="skills" />
			</div>
		</div>
		<div>
			<label>
				<spring:message code="view.create_job.duration" />
			</label>
			<div>
				<form:input id="duration" path="duration" type="text"
					/>
				<form:errors path="duration"/>
			</div>
		</div>
		<div class="form-group">
			<div>
				<input type="submit" id="Add" value="Add" />
			</div>
		</div>
		<div class="form-group">
			<div>
			<spring:url value="/welcome" var="mainCss" />
				<a href="${mainCss}"> 
					<input type="button" id="Cancel" value="Cancel"/>
				</a> 
				
			</div>
		</div>
	</form:form>

</body>
</html>