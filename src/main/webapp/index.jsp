<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
	<head>
		<meta charset="utf-8">
		<title>Welcome to the Digital Dervishes home</title>
	</head> 
	<body>
		<section>
		<div class="jumbotron">
			<div class="container"> 
			<img alt="none" src="/resources/images/Taj.jpg">
				<h1> <spring:message code="welcome.greeting"></spring:message> </h1>
				<p> <spring:message code="welcome.tagline"></spring:message> </p>
				<p> <security:authorize access="isAuthenticated()">
  					 <spring:message code="welcome.welcome"></spring:message> <security:authentication property="principal.username" />
				</security:authorize>

			</div>	 
 
				  <div class="container">
 				  <security:authorize access="isAnonymous()">
 				  <!-- WHICH ONE? depends on basic form OR CUSTOM -->
				</security:authorize>

		 
				</div>	
	 			<div class="pull-left"> <h3><spring:message code="welcome.SpecialBlurb"></spring:message></h3> </div>
		</div>	
	</section>
		<!-- <h3>Use Rest client to use this demo. If you use chrome click <a href="https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=en">here</a> </h3> -->
	<h3><a href="customers/list">Customers</a></h3>
	<h3><a href="dervishes/list">Dervishes</a></h3>
	<h3><a href="projects/list">Projects</a></h3>
	
	</body>
</html>
